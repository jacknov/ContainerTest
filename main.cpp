#include <string>
#include <vector>
#include <utility>
#include <chrono>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <list>

using namespace std;
using namespace chrono;

using write_sequence = vector<string>;

using test_pair = pair<uint64_t, string>;
using modify_sequence = vector<test_pair>;
using read_sequence = vector<test_pair>;

ifstream& operator >> (ifstream& _is, test_pair& _value)
{
    _is >> _value.first;
    _is >> _value.second;

    return _is;
}

template <typename S>
S get_sequence(const string& _file_name)
{
    ifstream infile(_file_name);
    S result;

    typename S::value_type item;

    while (infile >> item)
    {
        result.emplace_back(move(item));
    }

    return result;
}

class storage
{
public:
    ~storage() {
        for(const auto &e : v) {
            for(const auto &s : *e) {
                delete s;
            }
            delete e;
        }
    }
    void insert(const string& _str)
    {
        if(v.size() > 1) {
            if(*v[0]->front() > _str) {
                if(v[0]->size() < block_size) {
                    v[0]->insert(begin(*v[0]), new string{_str});
                    return;
                } else {
                    auto new_v = new vector<string *>{new string{_str}};
                    new_v->reserve(block_size + 1);
                    v.insert(begin(v), new_v);
                    return;
                }
            }
            auto start = begin(v);
            auto finish = end(v) - 1;
            for(auto it = start; it != finish; ++it) {
                if(*(*it)->front() <= _str && _str <= *(*(it + 1))->front()) {
                    auto insert_it = upper_bound(begin(**it), end(**it), _str,
                                                 [](const auto &l, const auto &r){return l < *r;});
                    if ((*it)->size() < block_size) {
                        (*it)->insert(insert_it, new string{_str});
                        return;
                    } else {
                        insert_it = (*it)->insert(insert_it, new string{_str});
                        auto new_v = new vector<string *>{insert_it, end(**it)};
                        new_v->reserve(block_size + 1);
                        it = v.insert(it + 1, new_v);
                        (*(it - 1))->erase(insert_it, end(**(it - 1)));
                        return;
                    }
                }
            }
            auto e = end(v) - 1;
            auto insert_it = upper_bound(begin(**e), end(**e), _str,
                                         [](const auto &l, const auto &r){return l < *r;});
            if((*e)->size() < block_size) {
                (*e)->insert(insert_it, new string{_str});
                return;
            } else {
                insert_it = (*e)->insert(insert_it, new string{_str});
                auto new_v = new vector<string *>{insert_it, end(**e)};
                new_v->reserve(block_size + 1);
                v.push_back(new_v);
                (*(end(v) - 2))->erase(insert_it, end(**(end(v) - 2)));
                return;
            }
        }
        if(v.empty()) {
            auto new_v = new vector<string *>;
            new_v->reserve(block_size + 1);
            new_v->push_back(new string{_str});
            v.push_back(new_v);
            return;
        }
        //случай одного элемента во внешнем векторе
        auto insert_it = upper_bound(begin(*v[0]), end(*v[0]), _str,
                [](const auto &l, const auto &r){return l < *r;});
        if(v[0]->size() < block_size) {
            v[0]->insert(insert_it, new string{_str});
            return;
        } else {
            insert_it = v[0]->insert(insert_it, new string{_str});
            auto new_v = new vector<string *>{insert_it, end(*v[0])};
        new_v->reserve(block_size + 1);
        v.push_back(new_v);
        v[0]->erase(insert_it, end(*v[0]));
        return;
    }

    //TODO insert str with sorting
}
void erase(uint64_t _index)
{
    auto it = begin(v);
    while (_index >= (*it)->size()) {
        _index -= (*it)->size();
        ++it;
    }
    (*it)->erase(begin(**it) + static_cast<long long>(_index));

    //TODO erase string via index
}
const string& get(uint64_t _index)
{
    auto it = begin(v);
    while (_index >= (*it)->size()) {
        _index -= (*it)->size();
        ++it;
    }

    return *(**it)[_index];

    //TODO return string via index
}
private:
//указатели быстрее перемещать, поэтому
//для ускорения сортировки использованы
//сырые указатели на векторы и указатели на строки
vector<vector<string *> *> v;
//экспериментальным путем установлено, что
//максимальная скорость работы алгоритма
//достигается при размере блока в 10 000 элементов
unsigned block_size{10000};
};

int main()
{
    write_sequence write =  get_sequence<write_sequence>("write.txt");
    modify_sequence modify =  get_sequence<modify_sequence>("modify.txt");
    read_sequence read = get_sequence<read_sequence>("read.txt");

    storage st;

    for (const string& item : write)
    {
        st.insert(item);
    }

    uint64_t progress = 0;
    uint64_t percent = modify.size() / 100;

    time_point<system_clock> time;
    nanoseconds total_time(0);

    modify_sequence::const_iterator mitr = modify.begin();
    read_sequence::const_iterator ritr = read.begin();

    for (; mitr != modify.end() && ritr != read.end(); ++mitr, ++ritr)
    {
        time = system_clock::now();
        st.erase(mitr->first);
        st.insert(mitr->second);
        const string& str = st.get(ritr->first);
        total_time += system_clock::now() - time;

        if (ritr->second != str)
        {
            cout << "test failed" << endl;
            return 1;
        }

        if (++progress % (5 * percent) == 0)
        {
            cout << "time: " << duration_cast<milliseconds>(total_time).count()
                 << "ms progress: " << progress << " / " << modify.size() << "\n";
        }
    }

    return 0;
}
